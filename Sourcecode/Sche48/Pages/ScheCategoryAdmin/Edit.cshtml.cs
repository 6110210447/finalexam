using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sche48.Data;
using Sche48.Models;

namespace Sche48.Pages.ScheCategoryAdmin
{
    public class EditModel : PageModel
    {
        private readonly Sche48.Data.Sche48Context _context;

        public EditModel(Sche48.Data.Sche48Context context)
        {
            _context = context;
        }

        [BindProperty]
        public ScheCategory ScheCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ScheCategory = await _context.ScheCategory.FirstOrDefaultAsync(m => m.ScheCategoryID == id);

            if (ScheCategory == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(ScheCategory).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ScheCategoryExists(ScheCategory.ScheCategoryID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool ScheCategoryExists(int id)
        {
            return _context.ScheCategory.Any(e => e.ScheCategoryID == id);
        }
    }
}
