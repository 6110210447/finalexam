using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Sche48.Data;
using Sche48.Models;

namespace Sche48.Pages.ScheCategoryAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly Sche48.Data.Sche48Context _context;

        public DeleteModel(Sche48.Data.Sche48Context context)
        {
            _context = context;
        }

        [BindProperty]
        public ScheCategory ScheCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ScheCategory = await _context.ScheCategory.FirstOrDefaultAsync(m => m.ScheCategoryID == id);

            if (ScheCategory == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ScheCategory = await _context.ScheCategory.FindAsync(id);

            if (ScheCategory != null)
            {
                _context.ScheCategory.Remove(ScheCategory);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
