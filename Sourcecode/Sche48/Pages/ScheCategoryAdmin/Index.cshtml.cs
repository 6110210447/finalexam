using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Sche48.Data;
using Sche48.Models;

namespace Sche48.Pages.ScheCategoryAdmin
{
    public class IndexModel : PageModel
    {
        private readonly Sche48.Data.Sche48Context _context;

        public IndexModel(Sche48.Data.Sche48Context context)
        {
            _context = context;
        }

        public IList<ScheCategory> ScheCategory { get;set; }

        public async Task OnGetAsync()
        {
            ScheCategory = await _context.ScheCategory.ToListAsync();
        }
    }
}
