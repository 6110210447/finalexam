using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Sche48.Data;
using Sche48.Models;

namespace Sche48.Pages.ScheAdmin
{
    public class CreateModel : PageModel
    {
        private readonly Sche48.Data.Sche48Context _context;

        public CreateModel(Sche48.Data.Sche48Context context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["ScheCategoryID"] = new SelectList(_context.ScheCategory, "ScheCategoryID", "ScheName");
            return Page();
        }

        [BindProperty]
        public Sche Sche { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.scheList.Add(Sche);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}