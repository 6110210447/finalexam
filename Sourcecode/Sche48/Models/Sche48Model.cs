﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Sche48.Models
{
    public class ScheUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }


    }
    public class ScheCategory
    {
        public int ScheCategoryID { get; set; }
        public string ScheName { get; set; }
        
    }
    public class Sche
    {
        public int ScheID { get; set; }

        public int ScheCategoryID { get; set; }
        public ScheCategory ScheCat { get; set; }

        [DataType(DataType.Date)]
        public string ScheDate { get; set; }
        public string ScheMonth { get; set; }
        public string ScheTime { get; set; }
        public string ScheStatus { get; set; }

        public string ScheUserId { get; set; }
        public ScheUser postUser { get; set; }
    }
    
}