﻿using Sche48.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Sche48.Data
{
    public class Sche48Context : IdentityDbContext<ScheUser>
    {
        public DbSet<Sche> scheList { get; set; }
        public DbSet<ScheCategory> ScheCategory { get; set; }

        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source= Sche48.db");
        }
    }
}






























































