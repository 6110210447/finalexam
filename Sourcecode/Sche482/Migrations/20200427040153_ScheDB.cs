﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sche48.Migrations
{
    public partial class ScheDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ScheCategory",
                columns: table => new
                {
                    ScheCategoryID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ScheName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ScheCategory", x => x.ScheCategoryID);
                });

            migrationBuilder.CreateTable(
                name: "newsList",
                columns: table => new
                {
                    ScheID = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    ScheCategoryID = table.Column<int>(nullable: false),
                    ScheDate = table.Column<string>(nullable: true),
                    ScheMonth = table.Column<string>(nullable: true),
                    ScheTime = table.Column<float>(nullable: false),
                    ScheStatus = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_newsList", x => x.ScheID);
                    table.ForeignKey(
                        name: "FK_newsList_ScheCategory_ScheCategoryID",
                        column: x => x.ScheCategoryID,
                        principalTable: "ScheCategory",
                        principalColumn: "ScheCategoryID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_newsList_ScheCategoryID",
                table: "newsList",
                column: "ScheCategoryID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "newsList");

            migrationBuilder.DropTable(
                name: "ScheCategory");
        }
    }
}
