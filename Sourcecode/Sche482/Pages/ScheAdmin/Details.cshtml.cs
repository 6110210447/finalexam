using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Sche48.Data;
using Sche48.Models;

namespace Sche48.Pages.ScheAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly Sche48.Data.Sche48Context _context;

        public DetailsModel(Sche48.Data.Sche48Context context)
        {
            _context = context;
        }

        public Sche Sche { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Sche = await _context.scheList
                .Include(s => s.ScheCat).FirstOrDefaultAsync(m => m.ScheID == id);

            if (Sche == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
