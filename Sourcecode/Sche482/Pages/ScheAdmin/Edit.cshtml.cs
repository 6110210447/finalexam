using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Sche48.Data;
using Sche48.Models;

namespace Sche48.Pages.ScheAdmin
{
    public class EditModel : PageModel
    {
        private readonly Sche48.Data.Sche48Context _context;

        public EditModel(Sche48.Data.Sche48Context context)
        {
            _context = context;
        }

        [BindProperty]
        public Sche Sche { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Sche = await _context.scheList
                .Include(s => s.ScheCat).FirstOrDefaultAsync(m => m.ScheID == id);

            if (Sche == null)
            {
                return NotFound();
            }
           ViewData["ScheCategoryID"] = new SelectList(_context.ScheCategory, "ScheCategoryID", "ScheCategoryID");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Sche).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ScheExists(Sche.ScheID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool ScheExists(int id)
        {
            return _context.scheList.Any(e => e.ScheID == id);
        }
    }
}
